/*
 *  Copyright 2014 David Edmundson <davidedmundson@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.0
import QtQuick.Particles 2.0
import QtMultimedia 5.0

Image {
    width: 800
    height: 600
    source: "../data/background.png"
    fillMode: Image.PreserveAspectCrop

    property bool playAudio: wallpaper.configuration.pew

    //foreground image of a cityscape that fireworks go behind
    Image {
        anchors.fill: parent
        source: "../data/foreground.png"
        fillMode: Image.PreserveAspectCrop
        z:1000
    }

    //REMEMBER, SPELL COLOUR INCORRECTLY
    //we want the fireworks to change colour but completely random looks rubbish, this is a list of colours to choose from
    property var fireworkColorMap: ['#FF9988','#77DD88','#9988FF'];

    SoundEffect {
        id: explosionSound
        //because sound effect caches sounds on compilation don't waste memory if we have sounds off
        source: playAudio ? "../data/totally_realistic_explosion_sound.wav" : ""
    }

    //ParticleSystem is a bit daft, even if there are no active particles it still
    //Obvioulsy a proper engineer would debug this in callgrind and fix it properly
    //so please ignore this code, and pretend I did that instead.
    Timer {
        id: powerSaveTimer
        interval: 2000
        onTriggered: particleSystem.running = false;
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            if (playAudio) {
                explosionSound.play();
            }

            particleSystem.running  = true;
            powerSaveTimer.restart();

            //choose a random colour from the list above
            var index = Math.floor(Math.random() * fireworkColorMap.length);
            particle.color = fireworkColorMap[index]
            
            //adjust first param to change how many big stars there are
            fireworkSource.burst(140, mouse.x, mouse.y);
        }
    }

    ParticleSystem {
        anchors.fill: parent
        id: particleSystem

        Emitter {
            id: fireworkSource
            group: "fireworkExplosion"
            size: 64
            endSize: 0
            lifeSpan: 1500
            lifeSpanVariation: 100
            enabled: false

            //burts spread out in all directions at magnitude 0 -> 140
            //variation makes it look 3D; i.e not moving much = must be moving towards you
            velocity: CumulativeDirection {
                AngleDirection {angle: 0; angleVariation: 360; magnitude: 70; magnitudeVariation: 70;}
            }
            //add gravity to things fall down a bit. This isn't just a half arsed non-realistic background
            acceleration: PointDirection {y:45}
        }

        ImageParticle {
            id: particle
            groups: ["fireworkExplosion"]
            source: "qrc:///particleresources/star.png"
            entryEffect: ImageParticle.Scale
            color: fireworkColorMap[0]
        }

        //super sexy trail behind the burst
        TrailEmitter {
            group: "trail"
            follow: "fireworkExplosion"
            emitRatePerParticle: 50
            emitShape: EllipseShape{}
            size: 2
            endSize: 0
            lifeSpan: 100
            lifeSpanVariation: 100

            emitWidth: 1

            //wiggling a bit makes trail look sparkly
            velocity: PointDirection {yVariation: 3; xVariation: 3}

            ImageParticle {
                system: particleSystem
                groups: ["trail"]
                source: "qrc:///particleresources/glowdot.png"

                entryEffect: ImageParticle.Fade
            }
        }
    }


}

